# Documentation TP CI/CD

## Projet utilisé

Testinfra :

Testinfra permet d'écrire des tests unitaires en Python pour tester l'état réel de vos serveurs configurés par des outils de gestion tels que Salt, Ansible, Puppet, Chef, etc.


Dans un premier temps, je voulais prendre un de mes propres sujet. Cependant après une multitude d'essais pour le faire fonctionner et le rendu qui s'approchait. J'ai utilisé un projet utilisé par un camarade afin d'avoir un projet qui marche et pouvoir l'intégrer à GitLab pour pouvoir par la suite créer des pipelines et avoir une intégration continue.

## Objectif du TP

L’objectif de TP est de mettre en fonctionnement une livraison continue sur le sujet choisi.

consignes : https://docs.google.com/document/d/1AtPzhCsW8xluITJSaSAqPFqPqhV54bwET__e3fIshHE/edit#

## Choix du système d'intégration continue

Le système d'intégration continue choisi est GitLab CI/CD.

J'ai choisi GitLab car c'est une plateforme open Source permettant de gérer et d'héberger des projets.

De plus, je trouve que contrairement aux autres plateforme d'intégration continue, GitLab offre une meilleure lisibilité des pipelines. GitLab nous permet de directement voir nos étapes du pipeline et de suivre ces dernières. L'interface principale nous permet d'avoir  les informations nécessaires sur les pipelines sans avoir à la quitter. Si des informations  supplémentaires sur une pipeline sont nécessaires, il suffit de cliquer dessus pour avoir les détails du déroulement du pipeline.

Pour finir,GitLab est l'une des plateformes que je maîtrise le mieux facilitant sa prise en main.


## Etape du pipeline

Il y a trois stages importants dans le pipeline que j'ai implémenté :
  - Build
  - Test
  - Deployement

#### Build
Le stage build est l'étape permettant au projet d'être "construit", c'est-à-dire, avoir toutes les cartes en main afin de pouvoir par la suite lancer le projet. Si le stage build ne passe pas alors le projet ne pourra pas être compilé et le reste des stages ne pourront pas être effectués.

Pour ce projet, le build utilise une image de python 3.7.

Le build contient plusieurs jobs qui permettent le bon fonctionnement de build :
  - flake8
  - pylint
  - sphinxdoc

Nous avons aussi le job pages, qui permet d'afficher la page de documentation afin de pouvoir prendre en main le projet.

###### Flake8 et Pylint8

 Ces deux étapes permettent de "linter le code ",c'est-à-dire, ils vont marcher comme un débogueur, ils vont analyser les fichiers sources du projet afin de détecter les erreurs qui sont dues à l’écriture du code et non à son exécution (les fuites mémoires).

###### Sphynxdoc

Shynxdoc permet de réaliser une belle et intelligente documentation.

#### Test
Le stage test est l'étape du pipeline, lorsque le projet est déjà builder, cela va permettre de faire tout les tests fonctionnels ou/et unitaire afin d'être sur du bon fonctionnement du projet avant de le déployer.


#### Deployement

 Le stage de deployement est le dernier stage et il permet  si tous les stages précédant ont été validé de déployer le projet. Ce stage est effectué uniquement si build et test sont validés.
 Ce stage permet le déploiement de la page de documentation que l'on peut retrouver  sur le lien.
 https://romaindelannet.gitlab.io/ci_cd_romain_delannet/master/html/

 Pour les branches, il n'y a pas de stage deployement. Cependant, une page de documentation est fourni sur le lien comme précédemment en changeant le master par le nom de la branche.

 ### Mon rendu

Mon rendu fournit des pipelines qui marchent et une page de documentation et fournis pour master et pour les branches. Cependant, les pages de documentation ne sont pas multiples selon master ou selon les branches. On obtient une page selon le dernier commit effectué avec un lien spécifiques /master/html pour master par exemple.

Ce TP m'a permis d'apprendre comment effectuer une intégration continue manuellement et comprendre les buts et les effets de chaque stage d'un pipeline. Et donc de comprendre le but de l'intégration continue.
